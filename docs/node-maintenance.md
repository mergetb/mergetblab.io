---
id: node-maintenance
title: Node Maintenance
---

This page describes how to conduct common node maintenance tasks in a Merge
facility.

## Power Control

Merge testbed facilities have a centralized node power control service called
[Beluga](https://gitlab.com/mergetb/tech/beluga). 

![](/img/node-maintenance/beluga.svg)

Beluga is an abstraction layer that hides the details of controlling the power
of devices through PDU, IPMI/Redfish or hypervisor specific mechanisms.

Usage of the `beluga` utility is straight forward.

```
beluga -h
```
```
Beluga client utility

Usage:
  beluga [command]

Available Commands:
  cycle       Cycle a device
  help        Help about any command
  list        List devices
  off         Turn off a device
  on          Turn on a device
  status      Get device power status
  version     Show beluga version

Flags:
  -h, --help   help for beluga

Use "beluga [command] --help" for more information about a command.
```

- `cycle` will turn a node off, wait for a short period then turn it on.
- `off` will turn a node off
- `on` will turn a node on

The power status of nodes may be queried via `status`

You can also show all available devices and what their underlying power control
mechanism is.

```
beluga list
```
```
a0    ipmi
a1    ipmi
a2    ipmi
a3    ipmi
m0    minnow
m1    minnow
mc0   apc
mc1   apc
n0    apc
n1    apc
n2    apc
```
