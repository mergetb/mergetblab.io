---
id: storage-example
title: Storage
---

**Note: Asset storage is not currently in production yet. (Last updated:
2021-08-04)**

We can also define filesystems where we can store experiment data and artifacts.
There is currently one type of storage (filesystems), with two types of
longevity (storage lifetime): experiment and site.  Experiment storage lives for
the duration of the experiment, and site storage lives for the lifetime of a
site.  These will be explained further in later sections, but for now, let us
look at an example of creating an experiment using storage in our experiment
description.

### Complete Example

```python
import mergexp as mx
from mergexp.unit import gb

# create a topology named storage
net = mx.Topology('storage')

# create a definition for a filesystem (fs) storage with a 10GB quota
siteAsset = mx.Storage(kind="fs", size=gb(10), name="staticAsset")

# create three nodes: a, b, and c and connect them on a LAN
nodes = [net.device(name) for name in ['a', 'b', 'c']]
lan = net.connect(nodes)

# statically assign each node with an ip address
for i,e in enumerate(lan.endpoints, 1):
  e.ip.addrs = ['10.0.0.%d/24' % i]

# mount our filesystem on each node under /mnt directory
# mount takes 2 arguments: where to mount, and the object to mount
for node in nodes:
  node.mount("/mnt/%s" % siteAsset.name, siteAsset)

mx.experiment(net)
```

There are two important parts of this code.  First is creating an asset:

```python
siteAsset = mx.Storage(kind="fs", size=gb(10), name="staticAsset")
```

Here we define an xir.Storage resource of type `fs` (filesystem), a quota
of 10gb, and a name of `staticAsset`.  That name is what will be referenced
when using the cli to get the details of the asset.

The second integral piece is that we need to tell the xir that while we've
create this asset, that we would like this asset to be apart of our
experiment, and for that we need to invoke `mount`:

```python
node.mount("/mnt/%s" % siteAsset.name, siteAsset)
```

Mount, tells the xir, that this asset will be attached to a node in our
experiment.  The first input to mount is the path to mount on the node,
in the exmaple above it would be `/mnt/[a,b,c]` depending on the node,
and the asset itself.  You can mount multiple assets to a node.
