---
id: mpl
title: Multipoint Links
---

A multipoint link (MPL) is a network element that interconnects multiple 
endpoints at layer-2. Examples of such elements include switches, hubs, 
and wireless access points. The collection of points interconnected by an MPL 
are often referred to as a broadcast domain as layer-3 address resolution is 
accomplished through broadcast messages.

The example below shows a simple MPL connecting 3 hosts.

![](/img/topo/lan3.png)

A multipoint link is not an explicitly modeled element. It's simply a link 
that happens to have more than 2 endpoints. In this example instead of
explicitly encoding the IP addresses like the [hello-world
example](/docs/hello-world), we've chosen to use the addressing reticulator that
will calculate a set of IP addresses for us.

### Complete Example

```python
import mergexp as mx
from mergexp.net import addressing, ipv4

net = mx.Topology('alpha', addressing == ipv4)

nodes = [net.device(name) for name in ['a', 'b', 'c']]
lan = net.connect(nodes)

mx.experiment(net)
```

