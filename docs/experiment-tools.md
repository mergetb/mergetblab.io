---
id: experiment-tools
title: Experimentation Tools
---

Merge has a dedicated Debian package server. The Merge packages are currently
distributed under the following Debian releases

- buster
- bullseye

While we do not provide explicit Ubuntu repositories at this time, the debian
repositoires are known to work for their Ubuntu counterparts e.g. buster ->
18.04, bullseye -> 20.04.

## Installation

The Merge package repository can be installed as follows.

### Add the Merge repo GPG key

```bash
sudo apt install gnupg2 curl
curl -L https://pkg.mergetb.net/gpg | sudo apt-key add
```

### Add the Merge repo to your apt config

Add the following to `/etc/apt/sources.list.d/merge.list`

#### Bullseye

```
deb [arch=amd64] https://pkg.mergetb.net/debian bullseye main
```

#### Buster

```
deb [arch=amd64] https://pkg.mergetb.net/debian buster main
```

### Update apt and install packages

```bash
sudo apt update
sudo apt install mergetb
```
