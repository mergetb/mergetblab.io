---
id: core-values
title: Core Values
---

Every line of code that goes into MergeTB at any level, from low level systems
code to DevOps automation should reflect the following values

## Simplicity

Many of the problems that Merge deals with are inherently complex. What striving
for simplicity means, is taking the time to understand the problem space you are
working in, identifying where the inherent complexities are, designing solutions
that contain the inherent complexity and provide simple easy to use interfaces 
to other parts of the system.

## Evolution

As a testbed platform, the role of MergeTB in the world is to provide a place
where the research and development community can understand the structure and
behavior of today's interconnected world and reach toward tomorrow through
experimentation. To do this effectively the MergeTB platform must be able to
evolve at the pace of technology. Concretely, this means that the way experiments
are _expressed_, _realized_ and _materialized_ must remain technology agnostic
. MergeTB provides a conduit for experimenters to access, provision and use
resources - but does not pre-determine what those resources are. In effect, this
means touch points with new technologies are added by experimenters and resource
providers and not MergeTB developers.

## Reliability

Think of MergeTB like you would a pair of ski bindings. When your ski
bindings are good, the fact that they are even there rarely if ever crosses your
mind, even though they serve such a critical function. They just do their job and
allow you to enjoy skiing. MergeTB should be like a good pair of ski bindings,
allow users to focus on the often intrinsically difficult task of developing
networked systems experiments, and never think about the set of testbeds
underpinning their experiment. Thus when working on Merge systems the mindset
should be one of preventing failure in the first place, or at the very least of
automatic recovery.

## Speed

Developing networked systems experiments has traditionally been a difficult and
time consuming task. It will remain difficult in many cases for the foreseeable
future, but it does not have to be intrinsically time consuming. One of the
initial things MergeTB did to support rapid experimentation was to separate the
[realization](/docs/experimentation#realization) 
of experiments from 
[materialization](/docs/experimentation#materialization). Allowing
users to rapidly define the constraints that create a viable validity
domain for their experiment. Follow on work brought experiment validity checkers
and reticulators. These _architectural_ features of MergeTB save the user large
amounts of time by establishing correctness as early as possible. When working
on Merge systems always think about how the _design_ of what you are doing
relates to the tempo of experimentation.
