# Website

This website is built using [Docusaurus 2](https://v2.docusaurus.io/), a modern static website generator.

The rendered website is available at:
- <https://mergetb.gitlab.io>
- <https://www.mergetb.org>

<!--
## Container Development

There is now a container which can be used to verify rendering.  Unlike modern web development
containers, this container does not have a watch function and is not mounted to the host, so
no changes are rendered while editing is being done.  Make sure to rebuild the container.

```
sudo make
```

The website will show up locally on your host machine on port `3000`.
-->

## Local Development

### Installation

If you don't already have a node setup, install it on Ubuntu via:
```
$ sudo apt install npm
```

Then, install the pre-requisites for this website with:
```
$ cd mergetb.gitlab.io
$ npm install
```

### Local Development Server

```
$ node run start
```

This command starts a local development server on
<http://localhost:3000>. Most changes are reflected live without having
to restart the server.

You can also pass in the host and port you want the local development
server to listen on:

```
$ npm run start -- --port 8000 --host 0.0.0.0
```

### Build

```
$ npm run build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

## Deployment

See <https://gitlab.com/mergetb/mergetb.gitlab.io/-/wikis/Updating-the-live-documentation-website>.
