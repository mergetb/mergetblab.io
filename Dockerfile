FROM debian:bullseye

MAINTAINER Geoff Lawler glawler@isi.edu

RUN apt-get update && apt-get install -y \
	npm \
	git

# copy over all our development files
# this is not a mount, so changes need re-run
COPY / /

# install dependencies
RUN npm install
# run npm build script
RUN npm run build

# cheeky, change npm start script to host for external
RUN sed -i 's/docusaurus start/docusaurus start --host 0.0.0.0/g' package.json

# expose default port
EXPOSE 3000

# run webserver
CMD ["npm", "run", "start"]
