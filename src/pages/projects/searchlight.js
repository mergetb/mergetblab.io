import React from 'react';
import Layout from '@theme/Layout';

function Searchlight() {
  return (
<Layout title="Searchlight Project">
<div style={{
      justifyContent: 'center',
      fontSize: '20px',
      marginLeft: 'auto',
      marginRight: 'auto',
      marginTop: '5em',
      width: '70%',
    }}>

<h2 id="case-studies">Case Studies in Experiment Design on a minimega Based Network Emulation Testbed</h2>
<p>Our scripts and documentation to run the experiments discussed in the <a href="https://cset21.isi.edu/">CSET 2021</a> paper "Case Studies in Experiment Design on a minimega Based Network Emulation Testbed" can be found on <a href="https://gitlab.com/mergetb/exptools/minimega-cset-2021">GitLab</a>.</p>
<p>Brian Kocoloski, Alefiya Hussain, Matthew Troglia, Calvin Ardi, Steven Cheng, Dave DeAngelis, Christopher Symonds, Michael Collins, Ryan Goodfellow, and Stephen Schwab. 2021. <em>Case Studies in Experiment Design on a minimega Based Network Emulation Testbed</em>. In Cyber Security Experimentation and Test Workshop (CSET '21). Association for Computing Machinery, New York, NY, USA, 83–90. DOI: <code>10.1145/3474718.3474730</code>.</p>
<p>GitLab Repository: <a href="https://gitlab.com/mergetb/exptools/minimega-cset-2021">https://gitlab.com/mergetb/exptools/minimega-cset-2021</a></p>
<p>Paper URL: <a href="https://doi.org/10.1145/3474718.3474730">https://doi.org/10.1145/3474718.3474730</a></p>
</div>

<div style={{
      justifyContent: 'center',
      fontSize: '20px',
      marginLeft: 'auto',
      marginRight: 'auto',
      marginTop: '5em',
      width: '70%',
    }}>
<h2 id="video-streaming">Building Reproducible Video Streaming Traffic Generators</h2>
<p>Our scripts and documentation to run the experiments discussed in the <a href="https://cset21.isi.edu/">CSET 2021</a> paper "Building Reproducible Video Streaming Traffic Generators" can be found on <a href="https://gitlab.com/mergetb/exptools/video-streaming">GitLab</a>.</p>
<p>Calvin Ardi, Alefiya Hussain, and Stephen Schwab. 2021. <em>Building Reproducible Video Streaming Traffic Generators</em>. In Cyber Security Experimentation and Test Workshop (CSET '21). Association for Computing Machinery, New York, NY, USA, 91–95. DOI: <code>10.1145/3474718.3474721</code>.</p>
<p>GitLab Repository: <a href="https://gitlab.com/mergetb/exptools/video-streaming">https://gitlab.com/mergetb/exptools/video-streaming</a></p>
<p>Paper URL: <a href="https://doi.org/10.1145/3474718.3474721">https://doi.org/10.1145/3474718.3474721</a></p>
</div>
</Layout>
);
}

export default Searchlight;
