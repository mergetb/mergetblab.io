module.exports = {
  title: 'MergeTB',
  tagline: 'Conduct experimental research around networked systems.',
  url: 'https://your-docusaurus-test-site.com',
  baseUrl: '/',
  favicon: 'img/merge.svg',
  organizationName: 'mergetb', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'MergeTB',
      logo: {
        alt: 'My Site Logo',
        src: 'img/logo.svg',
      },
      links: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {to: 'blog', label: 'Blog', position: 'left'},
        {
          href: 'https://gitlab.com/mergetb',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Learn',
          items: [
            {
              label: 'Experimentation',
              to: 'docs/',
            },
            {
              label: 'Providing Resources',
              to: 'docs/resources/',
            },
            {
              label: 'Operating A Portal',
              to: 'docs/portal/',
            },
            {
              label: 'Operating A Testbed Facility',
              to: 'docs/facility/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Slack',
              href: 'https://mergetb.slack.com',
            },
            {
              label: 'Forums',
              href: 'https://forum.mergetb.net',
            },
            {
              label: 'Mattermost',
              href: 'https://chat.mergetb.net', 
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/mergetb',
            },
            {
              label: 'Testbed Status',
              href: 'https://status.mergetb.net',
            }
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Information Sciences Institute`,
    },
    algolia: {
      apiKey: '264ba4fef86f16f82d135282cbcffea4',
      indexName: 'mergetb',
      contextualSearch: true,
      searchParameters: {},
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          homePageId: 'experimentation',
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/mergetb/mergetb.gitlab.io/-/blob/master',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/mergetb/mergetb.gitlab.io/-/blob/master',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
