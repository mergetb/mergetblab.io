all: run

build:
	docker build -t website:dev -f Dockerfile .
	#docker build --no-cache -t website:dev -f Dockerfile .

run: build
	docker run -p 3000:3000 website:dev
